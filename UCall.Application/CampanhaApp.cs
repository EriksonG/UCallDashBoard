﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCall.Application
{
    public class CampanhaApp : App
    {
        public Dictionary<int, string> ListarCampanhas()
        {
            var retorno = new Dictionary<int, string>();

            db.Campanha.ToList().ForEach(x =>
            {
                retorno.Add(x.Id_Campanha,x.NomeCampanha);
            });

            return retorno;
        }
    }
}
