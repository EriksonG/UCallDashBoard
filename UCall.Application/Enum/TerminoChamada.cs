﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCall.Application.Enum
{
    public enum TerminoChamada
    {
         Atendida=1,
         Ocupado=2,
         SemResposta=4,
         Abandonada=6,
         Rejeitada=7,
         NumeroInvalido=8,
         Sobrecarga=9,
         SobrecarganosTrunks=10,
         Rona=11
    }
}
