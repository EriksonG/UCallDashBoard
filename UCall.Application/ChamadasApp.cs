﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCall.Application
{
    public class ChamadasApp : App
    {
        public int ChamadasRecebidas(DateTime inicio, DateTime fim, int campanha)
        {
            return db.Chamada.Count(x => x.DataInicio >= inicio & x.DataInicio < fim & x.Id_Campanha == campanha);
        }

        public int ChamadasAtendidas(DateTime inicio, DateTime fim, int campanha)
        {
            return db.Chamada.Count(x => x.DataInicio >= inicio & x.DataInicio < fim & x.Id_Campanha == campanha & x.TipoTermino == 1);
        }

        public List<ResumoHora> RelatorioDiario(DateTime data, int campanha)
        {
            var retorno = new List<ResumoHora>();

            var dataFim = data.AddHours(24);

            var chamadas =  db.Chamada.Where(x => x.DataInicio > data & x.DataInicio < dataFim & x.Id_Campanha == campanha).ToList();

            for (int i = 0; i < 24; i++)
            {
                retorno.Add(new ResumoHora
                {
                    Intervalo = $"{i}:00 - {i}:59",
                    Chamadas = chamadas.Count(x=>x.DataInicio.Hour == i),
                    AtendidasUpper = chamadas.Count(x=> x.DataInicio.Hour == i & x.TipoTermino == 1 & x.TempoEspera > x.USL),
                    AtendidasLower = chamadas.Count(x => x.DataInicio.Hour == i & x.TipoTermino == 1 & x.TempoEspera < x.USL),
                    AbandonadasUpper = chamadas.Count(x => x.DataInicio.Hour == i & x.TipoTermino == 6 & x.TempoEspera > x.LSL),
                    AbandonadasLower = chamadas.Count(x => x.DataInicio.Hour == i & x.TipoTermino == 6 & x.TempoEspera < x.LSL),
                    Rejectadas = chamadas.Count(x => x.DataInicio.Hour == i & x.TipoTermino == 7),
                    Rona = chamadas.Count(x => x.DataInicio.Hour == i & x.TipoTermino == 11),
                    Sobrecargas = chamadas.Count(x => x.DataInicio.Hour == i & x.TipoTermino == 9),
                    PromedioTempoEspera = chamadas.Where(x=> x.DataInicio.Hour == i).Sum(x=>x.TempoEspera),
                    PromedioDuracaoChamada = chamadas.Where(x => x.DataInicio.Hour == i).Sum(x=>x.TempoEmLinha)
                });
            }

            return retorno;
        }

        public Dictionary<string, int> Top5ChamadasCompletas(DateTime inicio, DateTime fim, int campanha)
        {
            var resultado = new Dictionary<string, int>();

            var a = db.Chamada.Where(x => x.DataInicio >= inicio & x.DataInicio < fim & x.Id_Campanha == campanha & x.EasyCode != null).GroupBy(x=>x.Nivel3).Select(x=>new {x.Key , Count = x.Count()}).OrderByDescending(x=>x.Count).ToList();

            foreach (var t in a.Take(5))
            {
                resultado.Add(t.Key ?? "Sem Informações",t.Count);
            }
            
            resultado.Add("Outros",a.Skip(5).Sum(x=>x.Count));

            return resultado;
        }
    }



    public class ResumoHora
    {
        public string Intervalo { get; set; }

        public int Chamadas { get; set; }

        public int AtendidasUpper { get; set; }

        public int AtendidasLower { get; set; }

        public int AbandonadasUpper { get; set; }

        public int AbandonadasLower { get; set; }

        public int Rejectadas { get; set; }

        public int Sobrecargas { get; set; }

        public int Rona { get; set; }

        public int PromedioTempoEspera { get; set; }

        public int PromedioDuracaoChamada { get; set; }

        
    }
}
