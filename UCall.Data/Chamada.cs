namespace UCall.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Chamada")]
    public partial class Chamada
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id_Chamada { get; set; }

        public int? EasyCode { get; set; }

        public DateTime DataInicio { get; set; }

        public int TempoEspera { get; set; }

        public int TempoEmLinha { get; set; }

        public short TipoTermino { get; set; }

        public int Id_Campanha { get; set; }

        public bool ChamadaEntrante { get; set; }

        [StringLength(32)]
        public string DNIS { get; set; }

        [StringLength(30)]
        public string ANI { get; set; }

        [StringLength(50)]
        public string Nivel1 { get; set; }

        [StringLength(50)]
        public string Nivel2 { get; set; }

        [StringLength(50)]
        public string Nivel3 { get; set; }

        public short Id_Resultado { get; set; }

        [StringLength(20)]
        public string TipoChamada { get; set; }

        [StringLength(64)]
        public string RecordKey { get; set; }

        public int? Id_Agente { get; set; }

        public short USL { get; set; }

        public short LSL { get; set; }

        public virtual Agente Agente { get; set; }

        public virtual Campanha Campanha { get; set; }

        public virtual ResultadoChamada ResultadoChamada { get; set; }
    }
}
