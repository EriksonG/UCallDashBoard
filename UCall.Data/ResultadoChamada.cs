namespace UCall.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ResultadoChamada")]
    public partial class ResultadoChamada
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ResultadoChamada()
        {
            Chamada = new HashSet<Chamada>();
        }

        [Key]
        public short Id_Resultado { get; set; }

        [Required]
        [StringLength(20)]
        public string Resultado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Chamada> Chamada { get; set; }
    }
}
