namespace UCall.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelContext : DbContext
    {
        public ModelContext()
            : base("name=ModelConnection")
        {
        }

        public virtual DbSet<Agente> Agente { get; set; }
        public virtual DbSet<Campanha> Campanha { get; set; }
        public virtual DbSet<Chamada> Chamada { get; set; }
        public virtual DbSet<ResultadoChamada> ResultadoChamada { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agente>()
                .Property(e => e.Nome)
                .IsUnicode(false);

            modelBuilder.Entity<Campanha>()
                .Property(e => e.NomeCampanha)
                .IsUnicode(false);

            modelBuilder.Entity<Campanha>()
                .HasMany(e => e.Chamada)
                .WithRequired(e => e.Campanha)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Chamada>()
                .Property(e => e.DNIS)
                .IsUnicode(false);

            modelBuilder.Entity<Chamada>()
                .Property(e => e.ANI)
                .IsUnicode(false);

            modelBuilder.Entity<Chamada>()
                .Property(e => e.Nivel1)
                .IsUnicode(false);

            modelBuilder.Entity<Chamada>()
                .Property(e => e.Nivel2)
                .IsUnicode(false);

            modelBuilder.Entity<Chamada>()
                .Property(e => e.Nivel3)
                .IsUnicode(false);

            modelBuilder.Entity<Chamada>()
                .Property(e => e.TipoChamada)
                .IsUnicode(false);

            modelBuilder.Entity<Chamada>()
                .Property(e => e.RecordKey)
                .IsUnicode(false);

            modelBuilder.Entity<ResultadoChamada>()
                .Property(e => e.Resultado)
                .IsUnicode(false);

            modelBuilder.Entity<ResultadoChamada>()
                .HasMany(e => e.Chamada)
                .WithRequired(e => e.ResultadoChamada)
                .WillCascadeOnDelete(false);
        }
    }
}
