﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UCall.UI
{
    public static class Extras
    {
        public static string ToKwanza(this decimal valor)
        {
            return valor.ToString("F") + " Kz";
        }

        public static string ToPreco(this decimal valor)
        {
            return valor == 0 ? "Gratis" : valor.ToString("F") + " Kz";
        }

        public static string ToFecha(this DateTime valor)
        {
            return valor.ToString("dd/MM/yyyy hh:mm");
        }

        public static string ToFechaCorta(this DateTime valor)
        {
            return valor.ToString("dd/MM/yyyy");
        }

        public static string ToEstado(this bool estado)
        {
            if (estado)
                return "<label class='label label-success'>Activo<label/>";
            else
                return "<label class='label label-default'>Inativo<label/>";
        }

        public static string ToSimNao(this bool valor)
        {
            return valor ? "Sim" : "Não";
        }
    }
}