﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using UCall.UI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace UCall.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

            if (!roleManager.RoleExists("Estudante"))
                roleManager.Create(new IdentityRole("Estudante"));

            if (!roleManager.RoleExists("Docente"))
                roleManager.Create(new IdentityRole("Docente"));

            if (!roleManager.RoleExists("Secretaria"))
                roleManager.Create(new IdentityRole("Secretaria"));

            if (!roleManager.RoleExists("Parceiro"))
                roleManager.Create(new IdentityRole("Parceiro"));

            if (!roleManager.RoleExists("Contabilidade"))
                roleManager.Create(new IdentityRole("Contabilidade"));

            if (!roleManager.RoleExists("DirAcademica"))
                roleManager.Create(new IdentityRole("DirAcademica"));

            if (!roleManager.RoleExists("Admin"))
                roleManager.Create(new IdentityRole("Admin"));

            try
            {
                var user = new ApplicationUser()
                {
                    Nome = "Administrador",
                    UserName = "admin@hedgingconsult.com",
                    Email = "admin@hedgingconsult.com",
                    PhoneNumber = "000000000",
                    PicPath = "/assets/avatar/desconhecido.jpg",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                };
                var result = manager.Create(user, "admin123");
                if (result.Succeeded)
                {
                    manager.AddToRole(user.Id, "Admin");
                }
            }
            catch
            {
                // ignored
            }
        }

    }
}
