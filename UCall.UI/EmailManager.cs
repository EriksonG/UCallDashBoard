﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace UCall.UI
{
    public class EmailManager
    {
        public bool EnviarEmail(string destino, string asunto, string texto)
        {
            try
            {
                MailMessage mmsg = new MailMessage();
                mmsg.To.Add(new MailAddress(destino));
                mmsg.Subject = asunto;
                mmsg.SubjectEncoding = Encoding.UTF8;
                mmsg.Body = texto;
                mmsg.BodyEncoding = Encoding.UTF8;
                mmsg.IsBodyHtml = true;
                mmsg.From = new MailAddress("info@hc-school.com");
                SmtpClient cliente = new SmtpClient();
                cliente.Credentials = new NetworkCredential("info@hc-school.com", "UCall.UI2016#");
                cliente.Host = "mail.hc-school.com";
                cliente.Send(mmsg);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool EnviarEmail(List<string> destino, string asunto, string texto)
        {
            try
            {
                MailMessage mmsg = new MailMessage();
                destino.ForEach(x => mmsg.To.Add(x));
                mmsg.Subject = asunto;
                mmsg.SubjectEncoding = Encoding.UTF8;
                mmsg.Body = texto;
                mmsg.BodyEncoding = Encoding.UTF8;
                mmsg.IsBodyHtml = true;
                mmsg.From = new MailAddress("info@hc-school.com");
                SmtpClient cliente = new SmtpClient();
                cliente.Credentials = new NetworkCredential("info@hc-school.com", "UCall.UI2016#");
                cliente.Host = "mail.hc-school.com";
                cliente.Send(mmsg);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
    }
}