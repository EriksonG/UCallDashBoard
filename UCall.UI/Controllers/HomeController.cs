﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UCall.UI.Controllers;

namespace UCall.UI.Controllers
{
    public class HomeController : UCallController
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Erro()
        {
            var Ext = Server.GetLastError();
            return View(Ext);
        }
    }
}