﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Highsoft.Web.Mvc.Charts;
using Microsoft.Ajax.Utilities;
using UCall.Application;
using UCall.UI.Models;

namespace UCall.UI.Controllers
{
    public class ServiceLevelController : UCallController
    {
        
        private static CampanhaApp _campanhaApp;
        private static ChamadasApp _chamadasApp;

        public ServiceLevelController()
        {
            _campanhaApp = new CampanhaApp();
            _chamadasApp = new ChamadasApp();
        }

        public ActionResult Relatorio()
        {
            EnviarCampanhas(null);
            ViewBag.Resumos = new List<ResumoHora>();
            return View();
        }

        [HttpPost]
        public ActionResult Relatorio(RelatorioViewModel relatorio)
        {
            EnviarCampanhas(relatorio.IdCampanha);

            ViewBag.Resumos = _chamadasApp.RelatorioDiario(relatorio.Data, relatorio.IdCampanha);

            return View(relatorio);
        }


        public ActionResult Graficos()
        {
            EnviarCampanhas(null);
            return View();
        }

        [HttpPost]
        public ActionResult Graficos(RelatorioViewModel relatorio)
        {
            EnviarCampanhas(relatorio.IdCampanha);
            var resumos = _chamadasApp.RelatorioDiario(relatorio.Data, relatorio.IdCampanha);

            List<LineSeriesData> total = new List<LineSeriesData>();
            List<LineSeriesData> atendidas = new List<LineSeriesData>();
            List<LineSeriesData> abandonadas = new List<LineSeriesData>();
            List<LineSeriesData> rejectadas = new List<LineSeriesData>();
            List<LineSeriesData> rona = new List<LineSeriesData>();
            List<LineSeriesData> sobrecargas = new List<LineSeriesData>();

            resumos.Select(x => x.Chamadas).ForEach(x => { total.Add(new LineSeriesData { Y = x }); });
            resumos.Select(x => x.AtendidasLower + x.AtendidasUpper ).ForEach(x => { atendidas.Add(new LineSeriesData { Y = x }); });
            resumos.Select(x => x.AbandonadasLower + x.AbandonadasUpper).ForEach(x => { abandonadas.Add(new LineSeriesData { Y = x }); });
            resumos.Select(x => x.Rejectadas).ForEach(x => { rejectadas.Add(new LineSeriesData { Y = x }); });
            resumos.Select(x => x.Rona).ForEach(x => { rona.Add(new LineSeriesData { Y = x }); });
            resumos.Select(x => x.Sobrecargas).ForEach(x => { sobrecargas.Add(new LineSeriesData { Y = x }); });

            ViewData["total"] = total;
            ViewData["atendidas"] = atendidas;
            ViewData["abandonadas"] = abandonadas;
            ViewData["rejectadas"] = rejectadas;
            ViewData["rona"] = rona;
            ViewData["sobrecargas"] = sobrecargas;

            return View(relatorio);
        }

        private void EnviarCampanhas(int? idCampanha)
        {
            var campanhas = _campanhaApp.ListarCampanhas();
            ViewBag.IdCampanha = new SelectList(campanhas,"key","value",idCampanha);
        }
    }
}