﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace UCall.UI.Controllers
{
    [Erro]
    public class UCallController : Controller
    {
        public string UsuarioActual { get; private set; }

        public UCallController()
        {
            if (User != null)
                UsuarioActual = User.Identity.GetUserId();
        }

        public void MsgErro(string msg)
        {
            if ((List<string>)ViewBag.Erro == null)
            {
                ViewBag.Erro = new List<string> { msg };
            }
            else
            {
                ((List<string>)ViewBag.Erro).Add(msg);
            }
        }

        public void MsgOk(string msg)
        {
            if ((List<string>)TempData["Ok"] == null)
            {
                TempData["Ok"] = new List<string> { msg };
            }
            else
            {
                ((List<string>)TempData["Ok"]).Add(msg);
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        //protected JsonResultado AppToJson(AppResultado appRes)
        //{
        //    return new JsonResultado
        //    {
        //        Mensagem = appRes.Mensagem,
        //        Resultado = appRes.Resultado
        //    };
        //}

        //protected class JsonResultado
        //{
        //    public bool Resultado { get; set; }
        //    public string Mensagem { get; set; }
        //}
    }

    public class Erro : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;

            filterContext.ExceptionHandled = true;

            var model = new HandleErrorInfo(filterContext.Exception, "Controller", "Action");

            filterContext.Result = new ViewResult()
            {
                ViewName = "Erro",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}