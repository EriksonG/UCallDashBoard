﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Highsoft.Web.Mvc.Charts;
using UCall.Application;
using UCall.UI.Models;

namespace UCall.UI.Controllers
{
    public class DashboardController : UCallController
    {
        private static CampanhaApp _campanhaApp;
        private static ChamadasApp _chamadasApp;

        public DashboardController()
        {
            _campanhaApp = new CampanhaApp();
            _chamadasApp = new ChamadasApp();
        }
        // GET: Dashboard
        public ActionResult Index()
        {
            var fechatemp = DateTime.Now;

            var modelo = new DashboardViewModel
            {
                Campanha = 1,
                //DataInicio = new DateTime(fechatemp.Year, fechatemp.Month, 1),
                //DataFim = new DateTime(fechatemp.Year, fechatemp.Month, DateTime.DaysInMonth(fechatemp.Year, fechatemp.Month))

                DataInicio = new DateTime(2017,1,1),
                DataFim = new DateTime(2017,1,31)
            };

            modelo.DataFim = modelo.DataFim.AddDays(1).AddSeconds(-1);

            var pieData = new List<PieSeriesData>();

            var aaa = _chamadasApp.Top5ChamadasCompletas(modelo.DataInicio, modelo.DataFim, modelo.Campanha);

            foreach (var tipo in aaa)
            {
                pieData.Add(new PieSeriesData { Name = tipo.Key, Y = tipo.Value });
            }

            pieData[0].Sliced = true;
            pieData[0].Selected = true;
            
            ViewData["pieData"] = pieData;

            ViewData["Recebidas"] = _chamadasApp.ChamadasRecebidas(modelo.DataInicio, modelo.DataFim, modelo.Campanha);

            ViewData["Atendidas"] = _chamadasApp.ChamadasAtendidas(modelo.DataInicio, modelo.DataFim, modelo.Campanha);
            /*
            List<double?> johnValues = new List<double?> { 5, 3, 4, 7, 2 };
            List<double?> janeValues = new List<double?> { 2, 2, 3, 2, 1 };
            List<double?> joeValues = new List<double?> { 3, 4, 4, 2, 5 };

            List<ColumnSeriesData> johnData = new List<ColumnSeriesData>();
            List<ColumnSeriesData> janeData = new List<ColumnSeriesData>();
            List<ColumnSeriesData> joeData = new List<ColumnSeriesData>();

            johnValues.ForEach(p => johnData.Add(new ColumnSeriesData { Y = p }));
            janeValues.ForEach(p => janeData.Add(new ColumnSeriesData { Y = p }));
            joeValues.ForEach(p => joeData.Add(new ColumnSeriesData { Y = p }));

            ViewData["johnData"] = johnData;
            ViewData["janeData"] = janeData;
            ViewData["joeData"] = joeData;*/

            EnviarCampanhas(modelo.Campanha);

            return View(modelo);
        }

        [HttpPost]
        public ActionResult Index(DashboardViewModel modelo)
        {

            EnviarCampanhas(modelo.Campanha);
            return View(modelo);
        }

        private void EnviarCampanhas(int? idCampanha)
        {
            var campanhas = _campanhaApp.ListarCampanhas();
            ViewBag.IdCampanha = new SelectList(campanhas, "key", "value", idCampanha);
        }
    }
}