﻿$('.FormAjax').submit(function (event) {

    waiter = Splash();

    event.preventDefault();

    var $form = $(this), url = $form.attr('action');

    var posting = $.post(url, $form.serialize());

    posting.done(function (data) {
        if (data.Resultado === true) {
            MsgOk(data.Mensagem);
        }
        else{
            MsgErro(data.Mensagem);
        }

    });

    posting.fail(function () {
        MsgErro();
    });

    posting.always(function () {
        waiter.finish();
    });
});

$('.ModalFormAjax').submit(function (event) {

    waiter = Splash();

    event.preventDefault();

    var $form = $(this), url = $form.attr('action');

    var posting = $.post(url, $form.serialize());

    posting.done(function (data) {
        if (data.Resultado === true) {
            ModalMsgOk(data.Mensagem);
        }
        else {
            ModalMsgErro(data.Mensagem);
        }

    });

    posting.fail(function () {
        ModalMsgErro();
    });

    posting.always(function () {
        waiter.finish();
    });
});