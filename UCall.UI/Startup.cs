﻿using Microsoft.Owin;
using Owin;

//[assembly: OwinStartupAttribute(typeof(UCall.UI.Startup))]
namespace UCall.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
